////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_DIRSUPERVISOR_H
#define PROJECT_DIRSUPERVISOR_H

#include <iostream>
#include <thread>
#include <vector>

#include "DirObserver.h"

/**
 * This class is the top level construct that drives the
 * directory listening process. It spins off a new thread once
 * started and uses a polling methodology to watch a directory
 * for changes.
 */
class DirSupervisor
{
public:

    /**
     * Parameterized constructor.
     * @param pollingInterval: interval at which the directory is polled in seconds
     * @param dirObserver: a DirObserver object
     */
    DirSupervisor(unsigned int pollingInterval, DirObserver * dirObserver);
    ~DirSupervisor();

    /**
     * This function allows the user to watch multiple directories simply
     * by adding an additional observer object.
     * @param observer: DirObserver object
     */
    void addObserver(DirObserver * observer);

    /**
     * This function starts a new thread and begins the directory polling. Note that
     * the configured polling interval is the time period between the processing and
     * the next poll. This means that the processing may take longer than the configured
     * interval, but the idle time in between will always be what is configured.
     */
    void start();

    /**
     * This function stops the thread polling by setting a stop flag and joining the
     * thread back to main.
     */
    void stop();

    /**
     * This method allows the user to test to see if the supervisor is started or stopped.
     * @return
     */
    bool isStopped();

private:

    unsigned int _pollingInterval;
    volatile bool _isStopped;

    std::vector<DirObserver *> _dirObservers;
    std::thread * _supervisorThread;

    /**
     * This function is called by the thread.
     */
    void threadExec();
};

#endif //PROJECT_DIRSUPERVISOR_H
