////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_DIRLISTENER_H
#define PROJECT_DIRLISTENER_H

#include <iostream>
#include <filesystem>

/**
 * This class is the top level interface for the directory supervisor. The methods
 * in this class are called by the supervision process. It is the user's job to handle
 * the events as they come in.
 */
class DirListener
{
public:

    /**
     * This method is called when the supervisor detects a created file.
     * @param file: the created file
     */
    virtual void onFileCreate(std::filesystem::path & file) = 0;

    /**
     * This method is called when the supervisor detects a deleted file.
     * @param file: the deleted file
     */
    virtual void onFileDelete(std::filesystem::path & file) = 0;

    /**
     * This method is called when the supervisor detects a changed file.
     * @param file
     */
    virtual void onFileModified(std::filesystem::path & file) = 0;
};

#endif //PROJECT_DIRLISTENER_H
