////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_DIRLISTENERADAPTOR_H
#define PROJECT_DIRLISTENERADAPTOR_H

#include "DirListener.h"

/**
 * This class is an adaptor for the top level listener interface. It
 * is here to allow the user to choose which events to process, and
 * which to ignore.
 */
class DirListenerAdaptor : public DirListener
{
public:

    /**
     * Adapted method which implements the top level interface, but does nothing.
     * Called when a file is created.
     * @param file: the created file
     */
    void onFileCreate(std::filesystem::path & file) override { }

    /**
     * Adapted method which implements the top level interface, but does nothing.
     * Called when a file is deleted.
     * @param file: the deleted file
     */
    void onFileDelete(std::filesystem::path & file) override { }

    /**
     * Adapted method which implements the top level interface, but does nothing.
     * Called when a file is changed.
     * @param file: the modified file
     */
    void onFileModified(std::filesystem::path & file) override { }
};

#endif //PROJECT_DIRLISTENERADAPTOR_H
