////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_DIROBSERVER_H
#define PROJECT_DIROBSERVER_H

/**
 * Top level observer interface.
 */
class DirObserver
{
public:

    /**
     *  This function is meant to perform the filesystem delta
     *  checking and call the correct handlers.
     */
    virtual void doCheck() = 0;

};

#endif //PROJECT_DIROBSERVER_H
