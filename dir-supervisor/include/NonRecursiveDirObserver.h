////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef DIRSUPERVISOR_NONRECURSIVEDIROBSERVER_H
#define DIRSUPERVISOR_NONRECURSIVEDIROBSERVER_H

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "DirListener.h"
#include "DirObserver.h"

class NonRecursiveDirObserver : public DirObserver
{
public:

    NonRecursiveDirObserver(const std::string & topDir, DirListener * listener);
    ~NonRecursiveDirObserver();

    void doCheck() override;

private:

    std::string _topDir;
    DirListener * _listener;
    std::unordered_map<std::string, std::filesystem::file_time_type> _files;

    void doInit();
};

#endif //DIRSUPERVISOR_NONRECURSIVEDIROBSERVER_H
