////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include "DirSupervisor.h"

DirSupervisor::DirSupervisor(unsigned int pollingInterval, DirObserver * dirObserver)
{
    _pollingInterval = pollingInterval;
    _dirObservers.push_back(dirObserver);
    _supervisorThread = nullptr;
    _isStopped = true;
}

DirSupervisor::~DirSupervisor()
{

}

void DirSupervisor::start()
{
    if (_supervisorThread) // The polling thread is already started.
        return;

    _isStopped = false;
    _supervisorThread = new std::thread(&DirSupervisor::threadExec, this);
}

void DirSupervisor::stop()
{
    _isStopped = true; //Set the stop flag.
    _supervisorThread->join();
    _supervisorThread = nullptr;
}

void DirSupervisor::threadExec()
{
    while (!_isStopped)
    {
        for (const auto & observer : _dirObservers)
            observer->doCheck();

        std::this_thread::sleep_for(std::chrono::seconds(_pollingInterval));
    }
}

bool DirSupervisor::isStopped()
{
    return _isStopped;
}

void DirSupervisor::addObserver(DirObserver * observer)
{
    _dirObservers.push_back(observer);
}
