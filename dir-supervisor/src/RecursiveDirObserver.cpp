////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include "RecursiveDirObserver.h"

RecursiveDirObserver::RecursiveDirObserver(const std::string &topDir, DirListener * listener)
{
    _topDir = topDir;
    _listener = listener;
    this->doInit();
}

RecursiveDirObserver::~RecursiveDirObserver()
{

}

void RecursiveDirObserver::doInit()
{
    for (const auto & file : std::filesystem::recursive_directory_iterator(_topDir))
        _files.insert(std::pair<std::string, std::filesystem::file_time_type>(file.path().string(),
                std::filesystem::last_write_time(file)));

}

void RecursiveDirObserver::doCheck()
{
    // Check for deleted files:

    auto it = _files.begin();
    while (it != _files.end())
    {
        std::filesystem::path tempPath = it->first;
        if (!std::filesystem::exists(tempPath)) {
            _listener->onFileDelete(tempPath);
            it = _files.erase(it);
        }
        else
            it++;
    }

    // Check for changed or created files:

    for (const auto & file : std::filesystem::recursive_directory_iterator(_topDir))
    {
        std::filesystem::file_time_type fileModTime = std::filesystem::last_write_time(file);
        std::filesystem::path tempPath = file.path().string();

        if (_files.find(tempPath) == _files.end())
        {
            _files.insert(std::pair<std::string, std::filesystem::file_time_type>(tempPath, fileModTime));
            _listener->onFileCreate(tempPath);
        }
        else if (_files.at(tempPath) != fileModTime)
        {
            _files.at(tempPath) = fileModTime;
            _listener->onFileModified(tempPath);
        }
    }
}