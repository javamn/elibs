////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <iostream>

#include "DirSupervisor.h"
#include "DirObserver.h"
#include "DirListener.h"
#include "DirListenerAdaptor.h"
#include "RecursiveDirObserver.h"

int main ( void )
{


    class MyListener : public DirListenerAdaptor
    {
        void onFileCreate(std::filesystem::path & file) override {
            std::cout << "Hello";
        }
    };

    MyListener * listener = new MyListener();

    DirObserver * observer = new RecursiveDirObserver("./", listener);

    DirSupervisor * supervisor = new DirSupervisor(1, observer);

    return 0;
}