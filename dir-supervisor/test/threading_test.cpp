////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <iostream>
#include <thread>
#include <chrono>

#include "DirSupervisor.h"
#include "DirObserver.h"
#include "DirListener.h"
#include "DirListenerAdaptor.h"
#include "RecursiveDirObserver.h"
#include "NonRecursiveDirObserver.h"

int main ( void )
{

    class MyListener : public DirListenerAdaptor
    {
        void onFileCreate(std::filesystem::path & file) override {
            std::cout << "File Created: " << file.string() << "\n";
        }
        void onFileDelete(std::filesystem::path & file) override {
            std::cout << "File Deleted: " << file.string() << "\n";
        }
        void onFileModified(std::filesystem::path & file) override {
            std::cout << "File Modified: " << file.string() << "\n";
        }
    };

    MyListener * listener = new MyListener();

    DirObserver * observer = new RecursiveDirObserver("./", listener);
    DirObserver * n_observer = new NonRecursiveDirObserver("./", listener);

    DirSupervisor * supervisor = new DirSupervisor(1, observer);
    supervisor->addObserver(n_observer);

    std::cout << "starting the poller.\n";

    supervisor->start();

    if (supervisor->isStopped()) {
        std::cout << "Dir supervisor did not start.";
        return -1;
    }

    std::this_thread::sleep_for(std::chrono::seconds(5)); //Let the main thread sleep for a short while

    supervisor->stop();

    if (!supervisor->isStopped()) {
        std::cout << "Dir supervisor did not stop.";
        return -1;
    }

    // See if we can restart:
    std::cout << "\n\nTesting Restart \n\n";

    supervisor->start();

    if (supervisor->isStopped()) {
        std::cout << "Dir supervisor did not start.";
        return -1;
    }

    std::this_thread::sleep_for(std::chrono::seconds(5)); //Let the main thread sleep for a short while

    std::cout << "\n\nstopping.\n";

    supervisor->stop();

    if (!supervisor->isStopped()) {
        std::cout << "Dir supervisor did not stop.";
        return -1;
    }

    return 0;
}