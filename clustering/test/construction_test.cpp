////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <iostream>
#include <vector>

#include "dbscan.h"
#include "optics.h"

#include "InputReader.h"
#include "OutputWriter.h"

int main ( void )
{

    {
        std::cout << "-- Test 1 -- (Testing with 4 mock clusters)\n";
        InputReader * ir = new InputReader("./C_4.txt");
        std::vector<Point *> tempPoints = ir->getPoints();
        std::cout << "points size = " << tempPoints.size() << std::endl;

        DBSCAN * dbscan = new DBSCAN(0.8, 55, tempPoints);
        dbscan->useHaversineDistance(false);
        dbscan->run();
        std::unordered_map<unsigned int, std::vector<Point *>> tempClusters = dbscan->getClusters();

        std::cout << "Clusters = " << tempClusters.size() << std::endl;

        if (tempClusters.size() != 4) {
            std::cout << "Didn't identify correct clusters. Actual count = 4\n";
            return -1;
        }

        std::cout << "Writing out cluster files ...\n";

        for (const auto & i : tempClusters) {
            new OutputWriter("./t1_clus_" + std::to_string(i.first) + ".txt", i.second);
        }
    }

    {
        std::cout << "-- Test 2 -- (Testing with 3 mock clusters)\n";
        InputReader * ir = new InputReader("./C_3.txt");
        std::vector<Point *> tempPoints = ir->getPoints();
        std::cout << "points size = " << tempPoints.size() << std::endl;

        DBSCAN * dbscan = new DBSCAN(0.5, 50, tempPoints);
        dbscan->useHaversineDistance(false);
        dbscan->run();
        std::unordered_map<unsigned int, std::vector<Point *>> tempClusters = dbscan->getClusters();

        std::cout << "Clusters = " << tempClusters.size() << std::endl;

        if (tempClusters.size() != 3) {
            std::cout << "Didn't identify correct clusters. Actual count = 3\n";
            return -1;
        }

        std::cout << "Writing out cluster files ...\n";

        for (const auto & i : tempClusters) {
            new OutputWriter("./t2_clus_" + std::to_string(i.first) + ".txt", i.second);
        }
    }

    return 0;
}