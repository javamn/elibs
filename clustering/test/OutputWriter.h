////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef CLUSTERING_OUTPUTWRITER_H
#define CLUSTERING_OUTPUTWRITER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

/**
 * This is a dummy class to write a ssv point list.
 */
class OutputWriter
{
public:

    OutputWriter(std::string file, std::vector<Point *> tempPoints)
    {
        std::ofstream of(file);

        for (Point * p : tempPoints)
            of << p->getX() << " " << p->getY() << std::endl;
    }

private:

};

#endif //CLUSTERING_OUTPUTWRITER_H
