////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    
#ifndef CLUSTERING_INPUTREADER_H
#define CLUSTERING_INPUTREADER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>


class InputReader
{
public:
    InputReader(std::string filePath)
    {
        std::ifstream inFile;

        inFile.open(filePath);

        std::string line;
        double x, y;
        while (inFile >> x >> y)
        {
            std::cout << "x = " << x << ", y = " << y << std::endl;
            _inpPoints.push_back(new Point(x, y));
        }
    }

    std::vector<Point *> & getPoints()
    {
        return _inpPoints;
    }

private:

    std::vector<Point *> _inpPoints;

};

#endif //CLUSTERING_INPUTREADER_H
