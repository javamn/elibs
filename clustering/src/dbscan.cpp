////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////

#include "dbscan.h"

DBSCAN::DBSCAN(double eps, unsigned int minPts, std::vector<Point *> points)
{
    _eps = eps;
    _minPts = minPts;
    _neighborIdx.resize(points.size());

    for (Point * p : points) { // Wrap the point class with some extra fields.
        PointWrapper * t = new PointWrapper();
        t->p = p;
        t->classification = NOT_CLASSIFIED;
        t->neighbors = 0;
        _inputPoints.push_back(t);
    }
}

DBSCAN::~DBSCAN()
{

}

void DBSCAN::initializeHaversine()
{
    for (int i = 0; i < _inputPoints.size(); i++)
    {
        for (int j = 0; j < _inputPoints.size(); j++)
        {
            if (i == j) // We don't want to compare the same point with itself.
                continue;

            if (GeoMath::haversine(_inputPoints[i]->p, _inputPoints[j]->p) <= _eps)
            {
                _inputPoints[i]->neighbors++;
                _neighborIdx[i].push_back(j);
            }
        }
    }
}

void DBSCAN::initializeEuclid()
{
    for (int i = 0; i < _inputPoints.size(); i++)
    {
        for (int j = 0; j < _inputPoints.size(); j++)
        {
            if (i == j) // We don't want to compare the same point with itself.
                continue;

            if (GeoMath::euclidDistance(_inputPoints[i]->p, _inputPoints[j]->p) <= _eps)
            {
                _inputPoints[i]->neighbors++;
                _neighborIdx[i].push_back(j);
            }
        }
    }
}

void DBSCAN::run()
{
    if (_useHaversine) // If we are using geo-spatial distances, use haversine, else use euclid.
        this->initializeHaversine();
    else
        this->initializeEuclid();

    for (int i = 0, cIdx = 0; i < _inputPoints.size(); i++)
    {
        if (_inputPoints[i]->classification != NOT_CLASSIFIED) // This point has already been processed.
            continue;

        if (_inputPoints[i]->neighbors >= _minPts)
        {
            _inputPoints[i]->classification = CLASSIFIED;
            _clusters.insert(std::pair<unsigned int, std::vector<Point *>>(++cIdx, std::vector<Point *>()));

            _clusters[cIdx].push_back(_inputPoints[i]->p);

            for (int j = 0; j < _neighborIdx[i].size(); j++)
            {
                int nIdx = _neighborIdx[i][j];
                if (_inputPoints[nIdx]->classification != NOT_CLASSIFIED)
                    continue;

                _inputPoints[nIdx]->classification = CLASSIFIED;
                _clusters[cIdx].push_back(_inputPoints[nIdx]->p);
            }
            //std::cout << "--> Added cluster " << cIdx << ", with n_points " << _clusters[cIdx].size()
            // << ", reported " << _inputPoints[i]->neighbors << std::endl;
        }
        else
        {
            _inputPoints[i]->classification = NOISE;
        }
    }
}

std::unordered_map<unsigned int, std::vector<Point *>> & DBSCAN::getClusters()
{
    return _clusters;
}

void DBSCAN::useHaversineDistance(bool b)
{
    _useHaversine = b;
}


