######################
# Find elibs/geom
######################

SET(ELIBS ${CMAKE_SOURCE_DIR}/..)
SET(GEOM ${ELIBS}/geom)

SET(GEOM_INCLUDE ${GEOM}/include)

FIND_LIBRARY(GEOM_LIB
        NAMES
            Geom
        HINTS
            ${GEOM}/build/lib
)

IF( NOT GEOM_LIB )
    MESSAGE(FATAL_ERROR "Geom not found. Please build Geom first.")
ENDIF()

MESSAGE(STATUS "Found Geom: ${GEOM_LIB}")

##############################################

INCLUDE_DIRECTORIES( ${CMAKE_SOURCE_DIR}/include ${GEOM_INCLUDE} )

FILE( GLOB SOURCES "*.cpp" )

SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

ADD_LIBRARY( Clustering SHARED ${SOURCES} )

TARGET_LINK_LIBRARIES( Clustering  PUBLIC ${GEOM_LIB} )
TARGET_INCLUDE_DIRECTORIES( Clustering PUBLIC ${GEOM_INCLUDE} )