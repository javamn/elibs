////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_DBSCAN_H
#define PROJECT_DBSCAN_H

#include <iostream>
#include <vector>
#include <unordered_map>

#include "Point.h"
#include "GeoMath.h"

enum PointClassification {CLASSIFIED, NOT_CLASSIFIED, NOISE};

// Add extra fields to the points for the clustering routine.
typedef struct PointWrapper
{
    Point * p; // The point to wrap

    int classification; // Status flag for the computation.
    int neighbors; // Number of total point neighbors. (including noise)
} PointWrapper;

/**
 * This class performs DBSCAN (Density Based Spatial Clustering of Applications with Noise)
 * clustering.
 */
class DBSCAN
{
public:

    DBSCAN(double eps, unsigned int minPts, std::vector<Point *> points);
    ~DBSCAN();

    /**
     * This method is used to configure how the distance will be calculated.
     * If the user wants geo-spatial distances to be used, set this parameter
     * to true. If the user wants standard euclidean distances, set to false (default)
     * @param b useHaversine
     */
    void useHaversineDistance(bool b);

    /**
     * This method performs the DBSCAN clustering on the provided points.
     */
    void run();

    /**
     * This method will return a map of clusters after the computation is
     * complete. Each cluster has an unsigned int id and a vector of points.
     * @return map of cluster points
     */
    std::unordered_map<unsigned int, std::vector<Point *>> & getClusters();

private:
    double _eps;
    unsigned int _minPts;
    bool _useHaversine;
    std::vector<PointWrapper *> _inputPoints;
    std::vector<std::vector<int>> _neighborIdx;
    std::unordered_map<unsigned int, std::vector<Point *>> _clusters;

    /**
     * This method performs the nearest neighbor on all of the points.
     * If the haversine distance between the two compared points is less or equal
     * to the provided epsilon, the point is considered a neighbor.
     */
    void initializeHaversine();

    /**
     * This method performs the nearest neighbor on all of the points.
     * If the euclidean distance between the two compared points is less or equal
     * to the provided epsilon, the point is considered a neighbor.
     */
    void initializeEuclid();
};

#endif //PROJECT_DBSCAN_H
