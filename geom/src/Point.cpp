////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <Point.h>

#include "Point.h"

Point::Point()
{

}

Point::Point(double x, double y)
{
    _x = x;
    _y = y;
    _hasX = true;
    _hasY = true;
}

Point::Point(double x, double y, double z)
{
    _x = x;
    _y = y;
    _z = z;
    _hasX = true;
    _hasY = true;
    _hasZ = true;
}

Point::~Point()
{

}

void Point::setX(double x)
{
    _x = x;
    _hasX = true;
}

void Point::setY(double y)
{
    _y = y;
    _hasY = true;
}

void Point::setZ(double z)
{
    _z = z;
    _hasZ = true;
}

double Point::getX()
{
    return _x;
}

double Point::getY()
{
    return _y;
}

double Point::getZ()
{
    return _z;
}

bool Point::hasX()
{
    return _hasX;
}

bool Point::hasY()
{
    return _hasY;
}

bool Point::hasZ()
{
    return _hasZ;
}

bool Point::is2d()
{
    return (_hasX && _hasY);
}

bool Point::is3d()
{
    return (_hasX && _hasY && _hasZ);
}

double Point::getLon()
{
    return _x;
}

double Point::getLat()
{
    return _y;
}

double Point::getAlt()
{
    return _z;
}


