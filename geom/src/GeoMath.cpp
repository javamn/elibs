////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <GeoMath.h>

#include "GeoMath.h"

double GeoMath::haversine(Point *p1, Point *p2)
{
    if (!p1->is2d() || !p2->is2d())
        return -1;

    double lat1 = p1->getLat();
    double lon1 = p1->getLon();
    double lat2 = p2->getLat();
    double lon2 = p2->getLon();

    double dLat = (lat2 - lat1) * M_PI / 180.0;
    double dLon = (lon2 - lon1) * M_PI / 180.0;

    lat1 = (lat1) * M_PI / 180.0;
    lat2 = (lat2) * M_PI / 180.0;

    double a = pow(sin(dLat / 2), 2) +
            pow(sin(dLon / 2), 2) *
            cos(lat1) * cos(lat2);

    double c = 2 * asin(sqrt(a));

    return (GeoMath::EARTH_RADIUS_KM * c);
}

double GeoMath::euclidDistance(Point *p1, Point *p2) {
    if (!p1->is2d() || !p2->is2d())
        return -1;

    return sqrt(pow(p1->getX() - p2->getX(), 2) + pow(p1->getY() - p2->getY(), 2));
}
