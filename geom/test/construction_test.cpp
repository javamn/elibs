////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#include <iostream>

#include "test_util.h"
#include "Point.h"
#include "GeoMath.h"

int pointTest()
{
    Point * p = new Point();

    if (p->hasX() || p->hasY() || p->hasZ()) {
        std::cout << "empty point had incorrect has's.\n";
        return -1;
    }

    Point * p1 = new Point(1,2);

    if ((!p1->hasX() || !p1->hasY() || p1->hasZ()) &&
        !p1->is2d()) {
        std::cout << "x = " << p1->hasX() <<
                  "y = " << p1->hasY() <<
                  "z = " << p1->hasZ() << std::endl;
        std::cout << "2d point had incorrect has's.\n";
        return -1;
    }

    Point * p2 = new Point(1,2,3);

    if ((!p2->hasX() || !p2->hasY() || !p2->hasZ()) &&
        !p2->is3d()) {
        std::cout << "x = " << p2->hasX() <<
                  "y = " << p2->hasY() <<
                  "z = " << p2->hasZ() << std::endl;
        std::cout << "3d point had incorrect has's.\n";
        return -1;
    }

    Point * p3 = new Point();
    p3->setX(1);
    p3->setY(2);

    if (!p3->is2d()) {
        std::cout << "2d poing was not 2d\n";
        return -1;
    }

    ASSERT_EQUAL(p3->getX(), 1);
    ASSERT_EQUAL(p3->getY(), 2);

    Point * p4 = new Point();
    p4->setX(1);
    p4->setY(2);
    p4->setZ(3);

    if (!p4->is3d()) {
        std::cout << "3d point was not 3d\n";
        return -1;
    }

    ASSERT_EQUAL(p4->getX(), 1);
    ASSERT_EQUAL(p4->getY(), 2);
    ASSERT_EQUAL(p4->getZ(), 3);

    // If we are here, the test passed.
    return 0;
}

int haversineTest()
{
    Point * p1 = new Point(0.0, 0.0);
    Point * p2 = new Point(1.0, 1.0);

    double dist_km = GeoMath::haversine(p1, p2);

    ASSERT_EQUAL_FLOAT(dist_km, 157.25, 0.001);

    return 0;
}

int main ( void )
{
    if (pointTest() < 0) {
        std::cout << "\n\n\npoint test failed!\n\n";
        return -1;
    }

    if (haversineTest() < 0) {
        std::cout << "\n\n\nhaversine test failed!\n\n";
        return -1;
    }

    return 0;
}