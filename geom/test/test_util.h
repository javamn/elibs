////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef GEOM_TEST_UTIL_H
#define GEOM_TEST_UTIL_H

/**
 * x = actual value,
 * y = asserted value
 */
#define ASSERT_EQUAL(x,y) \
        if (x != y) { \
            std::cout << "Actual: " << x << std::endl; \
            return -1; \
            }
/**
 * x = actual value,
 * y = asserted value,
 * z = threshold
 */
#define ASSERT_EQUAL_FLOAT(x,y,z) \
        if (x > (y+z) || x < (y-z)) { \
            std::cout << "Actual: " << x << std::endl; \
            return -1;\
            }

#endif //GEOM_TEST_UTIL_H
