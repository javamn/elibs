////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef GEOM_GEOMATH_H
#define GEOM_GEOMATH_H

#include <cmath>

#include "Point.h"

class GeoMath
{
public:
    /*
     * Average radius of Earth
     */
    constexpr static double EARTH_RADIUS_KM = 6371.0088;

    /**
     * This function computers the haversine distance between
     * two points. The input points must be at least 2D.
     * @param p1: 2d point one
     * @param p2: 2d point two
     * @return the distance between the points in km
     */
    static double haversine(Point * p1, Point * p2);

    static double euclidDistance(Point * p1, Point * p2);
};

#endif //GEOM_GEOMATH_H
