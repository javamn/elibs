////////////////////////////////////////////////////////////////////////////    
//    Copyright (C) 2019 Evan R. Jaramillo
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////    

#ifndef PROJECT_POINT_H
#define PROJECT_POINT_H

class Point
{
public:

    Point();
    Point(double x, double y);
    Point(double x, double y, double z);
    ~Point();

    void setX(double x);
    void setY(double y);
    void setZ(double z);

    double getX();
    double getY();
    double getZ();

    double getLon();
    double getLat();
    double getAlt();

    bool hasX();
    bool hasY();
    bool hasZ();

    bool is2d();
    bool is3d();


private:

    double _x = 0.0,
           _y = 0.0,
           _z = 0.0;

    bool _hasX = false,
         _hasY = false,
         _hasZ = false;

};

#endif //PROJECT_POINT_H
